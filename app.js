// DOC HTML Lists
let docListTodos = document.getElementById("todos");
let docListDone = document.getElementById("done");

// DOC HTML Input field for new ToDos
let docNewTodo = document.getElementById("newTodo");

// File name
let jsonFile = "list.json";

// let localList = {};
let localTodos = [];
let localDone = [];

docListTodos.onclick = function(event) {
	// Select item clicked from DOC HTML Todos list,
	let clicked = event.target;
	// store text from clicked item
	let newDone = clicked.innerHTML;
	// remove it from todo list,
	localTodos.splice(localTodos.indexOf(newDone), 1);
	// add it to Dones list
	localDone.push(newDone);
	// reload
	loadList();
}

// Text from Input field
docNewTodo.addEventListener("keypress", function(e){
	if(e.key === "Enter") {
		let newTodo = docNewTodo.value;
		if(localTodos.includes(newTodo)) {
			alert("Already added!");
		}
		else if(localDone.includes(newTodo)) {
			alert("Already done!");
		} else {
			localTodos.push(newTodo);
			console.log(localTodos);
			loadList();
		}
	}
});

// Add local todos to DOC HTML list and reload
function loadList() {
		function addToList(localList, docList) {
			// Clear items in list
			docList.innerHTML = "";
			// Add items to list
			localList.forEach(element => {
				let listItem = document.createElement("li");
				listItem.innerHTML = element;
				docList.appendChild(listItem);
			});
		}
	addToList(localTodos, docListTodos);
	addToList(localDone, docListDone);
}

function readJson() {

	// When HTTP request ready..
	http.onreadystatechange = function() {
		
		// If request is correctly loaded
		if(http.readyState == 4 && http.status == 200) {

			// catch JSON response and put it in variable
			let jsonTodos = JSON.parse(http.response)

			// Sort Todos and Dones
			let todos = jsonTodos.todos;
			let done = jsonTodos.done;
			
			// Add todo's to To-do list
			todos.forEach(element => {
				localTodos.push(element)
			});

			// Add done's to Dones list
			done.forEach(element => {
				localDone.push(element);
			});

			// Verify Asynchronous function
			console.log("Asynchronous read succeeded");

			// reload DOC HTML list
			loadList();
			console.log(localTodos);
		}
	}
}

// Create HTTP request
let http = new XMLHttpRequest();

window.onload = function() {
	
	readJson();
	
	http.open("GET", jsonFile, true);
	http.send();
	
	// Proceed to inform client the request has already been sent
	console.log("JSON request has been sent");
};